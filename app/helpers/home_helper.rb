module HomeHelper
  def create_pictures_and_groups(urls, groups)
    urls.each do |url|
      Picture.create(url: url)
    end
    picture_groups = []
    groups = groups.split('][')
    groups.each do |group|
      @picture_group = PictureGroup.new marked: false
      group.split(',').each do |index_of_url|
        index_of_url = index_of_url.delete('[').delete(']').to_i
        @picture_group.pictures << Picture.find_by_url(urls[index_of_url - 1]) if urls[index_of_url - 1].present?
      end
      @picture_group.save if @picture_group.pictures.present?
      picture_groups << @picture_group if @picture_group.present?
    end
    picture_groups
  end
end
