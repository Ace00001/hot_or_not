require 'csv'
class Api::HomeController < Api::ApiController
  include HomeHelper
  before_action :authenticate_request

  def index
    render text: request.inspect
  end

  def project_results
    @project = Project.find(params[:project_id])
    results = CSV.generate do |csv|
      @project.picture_groups.where(marked:true).each do |picture_group|
        row = [picture_group.better.url]
        # this loop is used because picture_group.pluck(:url).join(',') puts an extra " at the end and beginning.
        picture_group.pictures.each do |picture|
          row.push picture.url
        end
        csv << row
      end
    end
    render text: results
  end

  def create_project
    if params[:urls].present? and params[:groups].present?
      @project = Project.new(title: params[:title], description: params[:description])
      @project = add_pictures(@project)
      render json: @project, include: :picture_groups
    else
      render text: 'Un Matched Params', status: 400
    end

  end

  def add_pictures_to_project
    if params[:project_id].present? and params[:urls].present? and params[:groups].present?
      @project = Project.find(params[:project_id])
      @project = add_pictures(@project)
      render json: @project, include: :picture_groups
    else

    end
  end

  def add_pictures(project)
    urls = params[:urls]
    groups = params[:groups]
    urls = urls.split(',')
    picture_groups = create_pictures_and_groups urls, groups
    project.picture_groups << picture_groups if picture_groups.present?
    project.save
  end

end