class Api::ApiController < ActionController::Base

  skip_before_action :verify_authenticity_token

  def authenticate_request
    puts '=========================================='
    puts 'Authenticating Request'
    puts request.headers['authentication'].inspect
    # Authenticate The InComing Requests Here.
    unless request.headers['authentication'] == 'hot-or-not'
      render text: 'Un Authenticated Request'
      return false
    end
    puts 'Valid Request'
    puts '=========================================='
  end
end