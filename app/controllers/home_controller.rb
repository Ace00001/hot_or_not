class HomeController < ApplicationController
  include HomeHelper
  include ActionView::Helpers::NumberHelper

  before_action :authenticate_user!, except: [:compare_pictures, :index, :save_result]
  def index
    @projects = Project.all
  end

  def compare_pictures
    @project = Project.find(params[:id])
    @picture_group = @project.picture_groups.where(marked: false).try(:first)
    redirect_to random_pictures_path if @picture_group.nil?
    @progress = (@project.picture_groups.where(marked: true).length.to_f / @project.picture_groups.length.to_f) * 100.0
    @progress = number_to_human @progress
  end

  def save_result
    @better = Picture.find(params[:better_id]) if params[:better_id].present?
    @picture_group = PictureGroup.find(params[:group_id]) if params[:group_id].present?
    @picture_group.update better_id: @better.id, marked:true if @picture_group.present?
    respond_to do |format|
      format.html { redirect_to compare_pictures_path(params[:project_id]) }
      format.js do
        @project = Project.find(params[:project_id])
            render json:  {project_id: @project.id, action_name: 'compare_pictures'}
      end
    end
  end

  def random_pictures
    @picture_group = PictureGroup.where(marked: false, project: nil).try :first
    puts @picture_group.inspect
  end

  def save_random_result
    @better = Picture.find(params[:better_id]) if params[:better_id].present?
    @picture_group = PictureGroup.find(params[:group_id]) if params[:group_id].present?
    @picture_group.update better_id: @better.id, marked: true if @picture_group.present?
    puts @picture_group.inspect
    respond_to do |format|
      format.html { redirect_to random_pictures_path(params[:project_id]) }
      format.js do
        render json:  {action_name: 'random_pictures'}
      end
    end
  end

  def add_pictures
    @picture = Picture.new
  end

  def save_pictures
    urls = params[:urls].split(',') unless params[:urls].nil?
    groups = params[:groups] unless params[:groups].nil?
    create_pictures_and_groups urls, groups
    redirect_to random_pictures_path
  end

  def new_project
    @project = Project.new
  end

  def edit_project
    @project = Project.find(params[:id])
  end

  def create_project
    @project = Project.new(project_params)
    urls = params[:urls].split(',') unless params[:urls].nil?
    groups = params[:groups] unless params[:groups].nil?
    @picture_groups = create_pictures_and_groups urls, groups
    @project.picture_groups << @picture_groups if @picture_groups.present?
    @project.save
    redirect_to compare_pictures_path(@project.id)
  end

  def update_project

  end

  private
  def project_params
    params.require(:project).permit(:title, :description)
  end

end
