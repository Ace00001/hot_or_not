class Picture < ActiveRecord::Base
  # has_many :worse_picture_comparisons, class_name: 'PictureComparison', :foreign_key => :better_id
  # has_many :worse_comparisons, :through => :worse_picture_comparisons, :source => :worse
  #
  # has_many :better_picture_comparisons, class_name: 'PictureComparison', :foreign_key => :worse_id
  # has_many :better_comparisons, :through => :better_picture_comparisons, :source => :better

  has_many :picture_group_associations, class_name: 'PictureGroupAssociation', foreign_key: 'picture_id'
  has_many :groups, class_name: 'PictureGroup', foreign_key: 'group_id', source: 'group'


end
