class Project < ActiveRecord::Base
  has_many :picture_groups, dependent: :destroy
end
