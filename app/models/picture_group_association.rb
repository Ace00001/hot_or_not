class PictureGroupAssociation < ActiveRecord::Base
  belongs_to :picture , class_name: 'Picture'
  belongs_to :group, class_name: 'PictureGroup', foreign_key: 'group_id'
end
