class PictureGroup < ActiveRecord::Base
  has_many :picture_group_associations, class_name: 'PictureGroupAssociation', foreign_key: 'group_id'
  has_many :pictures , through: 'picture_group_associations', class_name: 'Picture', foreign_key: 'group_id', dependent: :destroy, source: 'picture'
  belongs_to :better, class_name: 'Picture', foreign_key: 'better_id', dependent: :destroy
  belongs_to :project
end
