class CreatePictureGroupAssociations < ActiveRecord::Migration
  def change
    create_table :picture_group_associations do |t|
      t.integer :group_id
      t.integer :picture_id
      t.timestamps null: false
    end
  end
end
