class CreatePictureGroups< ActiveRecord::Migration
  def change
    create_table :picture_groups do |t|
      t.integer :better_id
      t.integer :project_id
      t.boolean :marked

      t.timestamps null: false
    end
  end
end
