class CreatePictureComparisons < ActiveRecord::Migration
  def change
    create_table :picture_comparisons do |t|
      t.integer :better_id
      t.integer :worse_id

      t.timestamps null: false
    end
  end
end
