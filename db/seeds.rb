# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
IMAGES_LIST = [
    [ { url: 'http://www.joomlaworks.net/images/demos/galleries/abstract/10.jpg'}, { url: 'http://www.joomlaworks.net/images/demos/galleries/abstract/1.jpg'}],
    [ { url: 'http://www.joomlaworks.net/images/demos/galleries/abstract/2.jpg'}, { url: 'http://www.joomlaworks.net/images/demos/galleries/abstract/3.jpg'}],
    [ { url: 'http://www.joomlaworks.net/images/demos/galleries/abstract/4.jpg'}, { url: 'http://www.joomlaworks.net/images/demos/galleries/abstract/5.jpg'}],
    [ { url: 'http://www.joomlaworks.net/images/demos/galleries/abstract/6.jpg'}, { url: 'http://www.joomlaworks.net/images/demos/galleries/abstract/7.jpg'}],
    [ { url: 'http://www.joomlaworks.net/images/demos/galleries/abstract/9.jpg'}, { url: 'http://www.joomlaworks.net/images/demos/galleries/abstract/8.jpg'}]
]

IMAGES_LIST.each do |group|
  pic_group = PictureGroup.new marked: false
  group.each do |picture|
    pic_number = picture[:url].split('/').last.to_i
    pic_group.pictures << Picture.create(url: picture[:url], picture_number: pic_number)
  end
  pic_group.save
  puts '================ Creating Picture Group ========================='
  puts pic_group.inspect
  puts pic_group.pictures.inspect
  puts '========================================================'

end