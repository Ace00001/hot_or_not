Rails.application.routes.draw do
  devise_for :users, controllers: { sessions: "users/sessions" }
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'home#index'

  get 'home/compare_pictures/:id' => 'home#compare_pictures', as: :compare_pictures
  get 'home/save_result/:better_id/:group_id/:project_id' => 'home#save_result', as: :save_result
  get 'home/add_pictures' => 'home#add_pictures', as: :add_pictures
  post 'home/save_pictures' => 'home#save_pictures', as: :save_pictures

  get 'home/new_project' => 'home#new_project', as: :new_project
  get 'home/edit_project' => 'home#edit_project', as: :edit_project

  post 'home/create_project' => 'home#create_project', as: :create_project
  post 'home/update_project' => 'home#update_project', as: :update_project

  get 'home/random_pictures' => 'home#random_pictures', as: :random_pictures
  get 'home/save_random_result/:better_id/:group_id' => 'home#save_random_result', as: :save_random_result

  #------------------------------API Routes----------------------------------------
  scope module: 'api' do
    get 'api/home/project_results/:project_id' => 'home#project_results', as: :api_project_results
    post 'api/home/create_project' => 'home#create_project', as: :api_create_project
    post 'api/home/add_pictures_to_project/:project_id' => 'home#add_pictures_to_project', as: :api_add_pictures
  end

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
